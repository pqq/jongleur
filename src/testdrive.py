"""
20.12.11 // pqq // njs

Test script that executes Jongleur's test functions
"""

import lib


try:
    lib.test_imports.do_import()

    lib.arguments.test_get()
    lib.error.test_output()
    lib.version.test_get()
    lib.verbose.test_output()

    print('All tests were successfully performed.')

except ValueError as value_error:
    import sys
    print(value_error.args[0] + ': ' + value_error.args[1])
    sys.exit(1)
