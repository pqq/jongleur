"""
Jongleur is a simple Python framework for building simple websites.

Work on this project started in December 2020, in the year of COVID-1984.
Jongleur is created by Frode Klevstul.

nifty:  `pylint --list-msgs | grep 'W0102'`
"""

import time
import types
import lib


def main():
    """
    Jongleur - main operations
    """

    # start the timer
    start_time = time.time()

    # create data_object to hold key data, for passing to the functions
    #
    # key data:
    # -----------------------------
    # data_object.config
    # data_object.config_file
    # data_object.content
    # data_object.layout
    # data_object.sections
    # data_object.verbose
    # -----------------------------
    data_object = types.SimpleNamespace()

    # get and set cli arguments
    arguments = lib.arguments.get()
    data_object.verbose = arguments.verbose
    data_object.config_file = arguments.config_file

    # print version details if in verbose mode
    if data_object.verbose:
        lib.version.output()

    # get and set config
    data_object.config = lib.reader.config(data_object)

    # get and set layout
    data_object.layout = lib.layout.load(data_object)

    # get and set content
    data_object.content = lib.reader.posts(data_object)

    # get and set sections
    data_object.sections = lib.builder.define_sections(data_object)

    # generate the pages
    lib.builder.generate_pages(data_object)

    # deploy the layout files to the export directory
    lib.layout.deploy(data_object)

    # print run time if in verbose mode
    lib.verbose.output('Run time: '+str(time.time()-start_time)+' seconds', 0, data_object.verbose)


if __name__ == '__main__':
    main()
