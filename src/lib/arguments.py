# pylint: disable=raise-missing-from
"""
20.12.11 // pqq // njs

Handle CLI arguments.
"""

import argparse
import traceback
import lib


def get(arg_for_test=None):
    """
    Get arguments
    """

    # create parser
    parser = argparse.ArgumentParser()

    # add arguments to parser
    parser.add_argument('-v', '--verbose', help="increase output verbosity", action="store_true")
    parser.add_argument('config_file')
    parser.add_argument(
        '--version', action='version', version='%(prog)s version '
        + lib.version.get()['number'] + ' (' + lib.version.get()['name'] + ')')

    # parse the arguments
    arguments = parser.parse_args(arg_for_test)

    return arguments


def test_get():
    """
    Test
    """
    try:
        _ = 1
        arguments = get(['-v', 'test.cfg'])
        if not arguments.verbose:
            raise ValueError(
                'arguments.get()',
                'verbose error \n------------------\n' +
                traceback.format_exc() + '------------------')
        if arguments.config_file != 'test.cfg':
            raise ValueError(
                'arguments.get()',
                'config file error \n------------------\n' +
                traceback.format_exc() + '------------------')
    except:
        raise ValueError(
            'arguments.get()',
            'other error \n------------------\n' +
            traceback.format_exc() + '------------------')
