# pylint: disable=too-many-locals
"""
20.12.31 // pqq // njs

Image handling.
"""

import base64
import math
import os
from bs4 import BeautifulSoup as BSHTML
from PIL import Image,ImageDraw,ImageFont
import lib


def photo_prepper(data_object, path_to_image):
    """
    Prepare images/photos by running `photoPrepper.sh`
    """

    os.system('bash ' + data_object.config['photoPrepper_path']
        + ' ' + data_object.config['photoPrepper_font']
        + ' ' + data_object.config['photoPrepper_watermark']
        + ' ' + str(data_object.config['photoPrepper_maxwidth'])
        + ' ' + str(data_object.config['photoPrepper_maxheight'])
        + ' ' + path_to_image)


def base64_encode(data_object, image_file_path):
    """
    Given the path to an image file, return it's Base64 eqvivalent representation for an HTML
    image's SRC tags (<img src="...">).
    """

    image_type = ''
    if any(ele in image_file_path for ele in ['jpg', 'JPG', 'jpeg', 'JPEG']):
        image_type = 'jpeg'
    elif any(ele in image_file_path for ele in ['png', 'PNG']):
        image_type = 'png'
    elif any(ele in image_file_path for ele in ['gif', 'GIF']):
        image_type = 'gif'
    elif any(ele in image_file_path for ele in ['bmp', 'BMP']):
        image_type = 'bmp'

    base64_encoded_image = ''

    try:
        image_content = lib.reader.file(image_file_path,'rb')
        lib.verbose.output('image processed ("'+image_file_path+'")', 9, data_object.verbose)
        encoded_image_string = base64.b64encode(image_content)
        base64_encoded_image = 'data:image/'+image_type+';base64,' + encoded_image_string.decode()

    except FileNotFoundError:
        lib.error.output('image.base64_encode(): Image not found: "' + image_file_path + '"')

    return base64_encoded_image


def asciify(data_object, path_to_input_image, path_to_output_image):
    """
    ASCIIfy image.

    https://medium.com/towards-artificial-intelligence/convert-images-to-ascii-art-images-using-python-90261de03c53
    """

    # Here Firstly the image is loaded then the scaling factor is chosen (0.1–1) and the size of
    # each ASCII character in the final output image is decided then the width and height of the
    # image are taken and the image is resized. Doing so is very important otherwise more
    # computational power will be used and the output images will be too big.

    try:
        image_object = Image.open(path_to_input_image)

        scale_factor    = data_object.config['asciify_scale_factor']
        char_width      = 10
        char_height     = 18
        width, height = image_object.size
        image_object = image_object.resize(
              (int(scale_factor*width)
            , int(scale_factor * height * (char_width/char_height)))
            , Image.NEAREST
        )
        width, height = image_object.size
        pixel_access = image_object.load()

        # Now the font for ASCII characters is decided. I have used inbuilt fonts of windows. After
        # that, a canvas ‘outputImage’ of black color is created using Image function and ImageDraw
        # object is created to embed ASCII characters on the canvas.

        font = ImageFont.truetype(data_object.config['asciify_font'], 15)
        output_image = Image.new('RGB', (char_width * width, char_height * height), color=(0,0,0))
        draw = ImageDraw.Draw(output_image)

        def get_some_char(height):
            """
            This function getSomeChar will get pixel value as a parameter that ranges between 0–256
            and it returns the respective ASCII character from the chars list. You can make your own
            char list if you want.
            """
            chars  = (
                r"$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1{}[]?-_+~<>i!lI;:,\"^`'. "[::-1]
            )
            char_list = list(chars)
            length = len(char_list)
            mul = length/256
            return char_list[math.floor(height*mul)]

        # This is where the most important work is done. We loop through each pixel and gathered RGB
        # components and the mean of them gives us the grey pixel value. Now, in a draw.text it
        # takes the position of embedding character as the first argument and character itself that
        # is being chosen with getSomeChar function as the second argument, a font that we have
        # chosen earlier as the third and fill that is color as the fourth argument.

        for i in range(height):
            for j in range(width):
                if len(pixel_access[j,i]) == 3:
                    red, green, blue = pixel_access[j,i]
                elif len(pixel_access[j,i]) == 4:
                    red, green, blue, _opacity = pixel_access[j,i]
                grey = int((red/3 + green/3 + blue/3))
                pixel_access[j,i] = (grey, grey, grey)
                draw.text((j*char_width, i*char_height), get_some_char(grey),
                    font=font,fill = (red, green, blue))

        output_image.save(path_to_output_image)

    except FileNotFoundError:
        lib.error.output('image.asciify(): Image not found: <' + path_to_input_image + '>')


def process(data_object, directory, html):
    """
    Main function to call for doing image processing.

    Note that processed images are named with an underscore (_) before the dot (.), like:
    image_.jpg

    Asciified images are named like:
    image-ascii.jpg

    Hence, a processed (prepped) version of an asciified image is named like:
    image-ascii_.jpg
    """

    def derive_processed_image_filename(img_src, do_asciify):
        """
        Derive a path to a processed version of the image given in img_src.
        In case we have image files with more than one dot, make sure to replace the last occurence
        of the dot with "_."
        """
        special_processed_name_part = '_.'
        if do_asciify:
            special_processed_name_part = '-ascii' + special_processed_name_part

        last_dot = img_src.rfind('.')

        return img_src[:last_dot] + special_processed_name_part + img_src[last_dot+1:]

    def photoprepp_and_asciify_image(directory, img_src, path_to_processed_image):
        """
        Check and possibly update the path to the image to open from the OS, for doing a photoPrepp.
        This function, or this update, is needed as the processed image might exist on the OS, or
        it might not. Hence, this function checks a few things. Then, photoPrepp and asciify images
        if needed.
        """

        # asciified images have their own naming as well
        last_dot = img_src.rfind('.')
        filename_asciified_image = img_src[:last_dot] + '-ascii.' + img_src[last_dot+1:]
        path_to_asciified_image = directory + filename_asciified_image

        # full path to be returned for base64 encoding outside this sub-function
        image_file_to_open = ''

        # we do not process the image if it will lead to double-processing.
        # example:
        #   normally, the md source includes an image like:
        #     ![](image.jpg)
        #   then, this will be replaced with a processed version with the name:
        #     image_.jpg
        #   however, if the source includes a processed image, directly, like:
        #     ![](image_.jpg)
        #   then we shall NOT create a double processed version of this, like:
        #     ![](image__.jpg)
        if '__.' in path_to_processed_image:
            image_file_to_open = directory + img_src

        # else if a processed version of the image already do exist we open/use the existing
        # processed image
        elif os.path.isfile(path_to_processed_image) and not data_object.config['photoPrepper_forceUpd']:
            image_file_to_open = path_to_processed_image

        # else if photoPrepper is enabled
        elif data_object.config['photoPrepper']:

            # we only asciify inside the photo prepper elif statement, this means that no
            # images will be asciified if the photoPrepper is not enabled.
            if data_object.config['asciify']:

                # create the asciified version of the image if it doesn't exists
                if not os.path.isfile(path_to_asciified_image):
                    asciify(data_object, directory + img_src, path_to_asciified_image)

                # use the asciified version of the image
                image_file_to_open = path_to_asciified_image

            else:
                image_file_to_open = directory + img_src

            # photoPrepp the image
            photo_prepper(data_object, image_file_to_open)

            # in case we have a processed/prepped asciify version we need another replace to point
            # to the processed/prepped asciify version (for base64 encoding outside this sub func).
            if data_object.config['asciify']:
                image_file_to_open = image_file_to_open.replace('-ascii.', '-ascii_.')
            # if not the image is asciified we shall base64 encode the processed image
            else:
                image_file_to_open = path_to_processed_image

        # else, in all other cases, read the image as it is
        else:
            image_file_to_open = directory + img_src

        return image_file_to_open

    # find all images in the given HTML code
    soup = BSHTML(html, features="html5lib")
    images = soup.findAll('img')

    # loop through all images
    for image in images:

        # if we are using an external image (as an url to the image)
        if image['src'].startswith('http'):
            continue

        # avoid processing already-base64 encoded images
        if image.get('id') == 'base64':
            continue

        processed_image_filename = (
            derive_processed_image_filename(image['src'], data_object.config['asciify']))

        # if the file name contains '/jongleur/' we assume the image file path is full, and no
        # directory is to be added in front of the filename
        if '/jongleur/' in image['src']:
            image_file_to_open = photoprepp_and_asciify_image(
                '', image['src'], processed_image_filename)

        # if the path doesn't contain '/jongleur/' we assume a relative path.
        else:
            image_file_to_open = photoprepp_and_asciify_image(
                directory, image['src'], directory + processed_image_filename)

        html = html.replace('"'+image['src']+'"', '"'+base64_encode(data_object, image_file_to_open)+'"')

    return html
