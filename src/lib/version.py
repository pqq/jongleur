"""
20.12.11 // pqq // njs

Handeling of Jongleur's version information.
"""

import traceback
import lib


def get():
    """
    Return version name and number.
    """
    version_details = {
        'number':     '1',
        'name':       'Janker'
    }

    return version_details


def test_get():
    """
    Test.
    """
    try:
        ver = get()
    except ValueError as value_error:
        raise ValueError(
            'version.test_get()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------') from value_error
    try:
        _ver_no = ver['number']
    except ValueError as value_error:
        raise ValueError(
            'version.test_get()',
            'unable to get version number\n------------------\n' +
            traceback.format_exc() + '------------------') from value_error


def output():
    """
    Print version details.
    """
    version_details = get()

    lib.verbose.output('-----------------------------------', 0, True)
    lib.verbose.output('Jongleur v'+version_details['number']
        + ' ('+version_details['name']+')', 0, True)
    lib.verbose.output('-----------------------------------', 0, True)
