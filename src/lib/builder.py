"""
20.12.13 // pqq // njs

Functions related building the website.
"""

import os
import shutil
import types
import lib


def process_assets(data_object, page_object, layout):
    """
    Handle the post page's assets directory, and related links.
    """

    assets_src = page_object.dir_path + 'assets/'
    if os.path.isdir(assets_src):
        layout = layout.replace(data_object.config['assets_tag'], 'assets/'+page_object.filename+'/')
        assets_trg = data_object.config['root'] + data_object.config['export_dir'] + '/assets/' + page_object.filename + '/'
        shutil.copytree(page_object.dir_path+'assets/', assets_trg, dirs_exist_ok=True)

    return layout


def define_sections(data_object):
    """
    Given a data_object with a list of content, define the sections for the site.
    """

    # the the name of the home section is defined in the config. add that first.
    sections = [data_object.config['header_home']]

    # loop through all posts, and derive section name from the path
    for res in data_object.content:
        directory = str(res).replace(data_object.config['root']
            + lib.constants.DIR_SECTIONS + '/', '').split( '/' )

        if not directory[0] in sections:
            sections.append(directory[0])

    lib.verbose.output(
        str(len(sections)) + ' sections defined ('+str(sections)+')', 0, data_object.verbose)

    return sections


def posts_for_section(data_object, section):
    """
    Return a sorted list of paths to posts for a given section.
    """

    list_of_posts = []

    # loop through all markdown files, given as a list
    for path_to_md_file in sorted(data_object.content, reverse=data_object.config['desc_post_sorting']):

        current_file_dir = path_to_md_file.replace(data_object.config['root'] + lib.constants.DIR_SECTIONS + '/', '')

        # get the posts for the given section
        if current_file_dir.startswith(section):

            # append posts to the post lists
            list_of_posts.append(path_to_md_file)

    return list_of_posts


def home_page(data_object):
    """
    Build the site's home page.
    """

    # create a page object that will hold relevant data for creating the page
    page_object = types.SimpleNamespace()

    # define full path for the home page
    home_page_path = (
        data_object.config['root'] + data_object.config['export_dir'] + '/'
        + lib.constants.FILE_HOME_OUT)

    # set filename
    page_object.filename = lib.constants.FILE_HOME_OUT

    # verbose mode output
    lib.verbose.output(data_object.config['header_home'].upper(), 3, data_object.verbose)
    lib.verbose.output('> ' + home_page_path, 6, data_object.verbose)

    # generate the menu, with the home page as selected menu element/button
    page_object.menu = lib.layout.menu(data_object, data_object.config['header_home'])

    # set main content, using the content found in the predetermined "home.md" file
    home_content = lib.reader.file(data_object.config['root'] + lib.constants.FILE_HOME_IN)

    page_object.header_sub = ''
    if '~~~\n' in home_content:
        page_object.header_sub, home_content = home_content.split('~~~\n')

    page_object.content = lib.markd.htmlify(
              data_object
            , data_object.config['root'] + lib.constants.DIR_HOME + '/'
            , home_content
        )

    # generate the layout for the home page
    layout = lib.layout.page(data_object, page_object, 'home')

    # open, write to and close file
    file_home = open(home_page_path, 'w')
    file_home.write(layout)
    file_home.close()


def overview_page(data_object, section, menu, list_of_posts):
    """
    Build the section overview page. This page will contain a list of links to all related content.
    """

    lib.verbose.output('> '+ data_object.config['export_dir'] + '/' + section + '.html', 6, data_object.verbose)

    page_object = types.SimpleNamespace()
    content = ''

    # store the section in the page_object
    page_object.section = section

    # store menu in page_object
    page_object.menu = menu

    # set the path for the overview page
    overview_page_path = (data_object.config['root']
        + data_object.config['export_dir'] + '/' + section + '.html')

    # set filename
    page_object.filename = section + '.html'

    # get and include the overview page's text content, if it exists
    overview_content_file_start = f'{data_object.config["root"]}{lib.constants.DIR_SECTIONS}/{section}/'
    file_overview_content = lib.constants.FILE_OVERVIEW_CONTENT

    # if a site specific overview content page is given
    if (data_object.config['file_overview_content']
        and os.path.isfile(overview_content_file_start+data_object.config['file_overview_content'])):
        file_overview_content = data_object.config['file_overview_content']

    overview_content_file = (data_object.config['root'] + lib.constants.DIR_SECTIONS + '/' + section + '/' + file_overview_content)

    if os.path.isfile(overview_content_file):
        content = lib.markd.htmlify(
                      data_object
                    , data_object.config['root'] + lib.constants.DIR_SECTIONS + '/' + section + '/'
                    , lib.reader.file(overview_content_file)
                )

    # set the content of the page_content object
    page_object.content = content

    # add list of posts for current section to the overview page
    page_object.posts = []
    for post in list_of_posts:
        page_object.posts.append(post)

    # generate the layout for the overview page
    layout = lib.layout.page(data_object, page_object, 'overview')

    # open, write to and close file
    file_overview = open(overview_page_path, 'w')
    file_overview.write(layout)
    file_overview.close()


def post_page(data_object, menu, path_to_md_file):
    """
    Build a single post page. Some of the post's data is returned, and will be used when building
    the overview page.
    """

    # create page_object to hold key data
    #
    # key data:
    # -----------------------------
    # page_object.dir_path
    # page_object.relative_path
    # page_object.filename
    # page_object.content
    # -----------------------------
    page_object = types.SimpleNamespace()
    return_content = None

    # store menu in page_object
    page_object.menu = menu

    # retrieve info for the given path
    path_details = lib.misc.pathify(data_object, path_to_md_file)
    page_object.dir_path = path_details['dir_path']
    page_object.relative_path = path_details['relative_path']
    page_object.filename = path_details['html_filename']

    # read md file from the os
    page_object.content = lib.reader.post(data_object, path_to_md_file)

    if not page_object.content:
        return return_content

    # only process posts that are published
    if page_object.content['published'] == 'true':

        # verbose output
        lib.verbose.output(str(page_object.relative_path)+' > '
        + data_object.config['export_dir']+'/'+page_object.filename, 6, data_object.verbose)

        # get content for the page that shall be returned to be listed on the overview page
        return_content = (
            lib.layout.post_listing(
                data_object, page_object, data_object.config['llayout_overview_page']))

        # build post page
        layout = lib.layout.page(data_object, page_object, 'post')

        # process assets
        layout = process_assets(data_object, page_object, layout)

        # open, write to and close file
        file_post = open(data_object.config['root']
            + data_object.config['export_dir'] + '/' + page_object.filename, 'w')
        file_post.write(layout)
        file_post.close()

    return return_content


def generate_pages(data_object):
    """
    Main builder function that generate HTML pages.
    """

    # clean export directory for avoiding old outdated pages getting stuck
    lib.misc.empty_export_dir(data_object)

    html = ''

    lib.verbose.output('Generate pages:', 0, data_object.verbose)

    # build home page
    home_page(data_object)

    # loop through all sections
    for section in data_object.sections:

        # the home section (page) has been built outside of this loop
        if section == data_object.config['header_home']:
            continue

        lib.verbose.output(section.upper(), 3, data_object.verbose)

        # build the menu
        menu = lib.layout.menu(data_object, section)

        # get a list of posts for the given section
        list_of_posts = posts_for_section(data_object, section)

        # generate the post pages, and append the returned content for the building of the
        # overview page
        post_listings_for_overview = []
        for path_to_post in list_of_posts:
            post_listings_for_overview.append(post_page(data_object, menu, path_to_post))

        # build the overview page
        overview_page(data_object, section, menu, post_listings_for_overview)

    # create sitemap
    if data_object.config['sitemap']:
        lib.sitemap.create(data_object)

    # generate feed files
    if data_object.config['feed']:
        lib.feed.create(data_object)

    return html
