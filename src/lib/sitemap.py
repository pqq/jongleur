"""
21.03.12 // pqq // njs

Sitemap generator, following the standard defined at:
https://www.sitemaps.org/protocol.html
"""

import datetime
import lib


def load(data_object):
    """
    Load and return the template sitemap file, 'sitemap.xml'.
    """

    file_to_load = data_object.config['root'] + lib.constants.FILE_SITEMAPXML

    lib.misc.check_file_existence(file_to_load, True)

    lib.verbose.output('Sitemap file loaded ("'+file_to_load+'")', 4, data_object.verbose)

    return lib.reader.file(file_to_load)


def generate(data_object):
    """
    Generate sitemap.
    """

    exported_html_files = lib.reader.exported_html(data_object)

    url_set = ''

    for file in exported_html_files:
        if data_object.config['sitemap_exclude_hidden']:
            if (str(file).rsplit('/', 1)[1]).count('.') > 1:
                continue

        url_set += '<url>\n'
        url_set += (
            '  <loc>' + data_object.config['base_url'] + '/' + str(file).rsplit('/', 1)[1]
            + '</loc>\n')
        url_set += ('  <lastmod>' + datetime.datetime.now().strftime('%Y-%m-%d')+ '</lastmod>\n')
        url_set += '</url>\n'

    return url_set


def write(data_object, sitemap_template, url_set):
    """
    Write the sitemap to the export directory.
    """

    lib.verbose.output('Creating sitemap.xml', 4, data_object.verbose)

    with open(data_object.config['root'] + data_object.config['export_dir'] + '/sitemap.xml'
        , 'w', encoding='utf-8') as sitemap_file:
        sitemap_file.write(sitemap_template.replace('[URLSET]', url_set))


def create(data_object):
    """
    Create sitemap for current site.
    """

    lib.verbose.output('Creating sitemap:', 0, data_object.verbose)

    sitemap_template = load(data_object)
    url_set = generate(data_object)
    write(data_object, sitemap_template, url_set)
