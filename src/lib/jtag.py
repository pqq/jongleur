"""
21.01.06 // pqq // njs

Functions related to the handling of the JTAG (Jongleur Tag).
"""

import re
import types
import lib


def get_name(data_object, jtag_full):
    """
    Given a JTAG, return its name.
    """

    return (
        jtag_full.replace(
            data_object.config['jtag_start'], '').split('=')[0].split('|')[0].split(',')[0]
    )


def get_size(data_object, jtag_full):
    """
    Given a JTAG, return the size (of the list to get). This is an optional value/digit after
    pipe (|) and before equal (=) or comma (,)).

    Example, of a JTAG with a count of 2:
    {{LSG|2=album}}
    """

    return (
             int(jtag_full.split('|')[1].split('=')[0].split(',')[0]) if '|' in jtag_full
        else int(data_object.config['list_max_posts'])
    )


def get_start_index(jtag_full):
    """
    Given a JTAG, return the size (of the list to get). This is an optional value/digit after
    pipe (|) and before equal (=)).

    Example, of a JTAG with a count of 2:
    {{LSG|2=album}}
    """

    return (
             int(jtag_full.split(',')[1].split('=')[0]) if ',' in jtag_full
        else 1
    )


def get_value(data_object, jtag_full):
    """
    Given a JTAG, return its value.
    """

    return jtag_full.split('=')[1].replace(data_object.config['jtag_end'], '')


def load_external_content(url):
    """
    Loading external content.
    """

    url_stripped, _, parameters = url.partition('::')

    html_object = lib.www.fetch(url_stripped)

    # example of url with parameters
    # {{LEC=https://someurl.com/heya.md::[email][E-post]::[name][Navn]::(JCON_1)}}

    # special parameters are split by "::"
    param_array = parameters.split('::')

    for entry in param_array:
        # if [name][value]
        if '][' in entry:
            name, value = entry.split('][')
            name = name[1:]
            value = value[:-1]
            html_object = html_object.replace(f'[{name}]', value)
        # if JCON / jongleur condition
        elif entry.startswith('(') and entry.endswith(')'):
            entry = entry[1:-1]
            jcon, jcon_no = entry.split('_')
            html_object = html_object.replace(f'[{jcon}_{jcon_no}]', '')
            html_object = html_object.replace(f'[{jcon_no}_{jcon}]', '')

    # comment out remaining jcon parts
    html_object = html_object.replace('[JCON', '<!--[JCON')
    html_object = html_object.replace('JCON]', 'JCON]-->')


    return html_object


def jtag_load_internal_post(data_object, relative_path):
    """
    Load internal post.
    """

    path_to_md_file = lib.misc.to_full_path(data_object, relative_path)

    post = lib.reader.post(data_object, path_to_md_file)

    # if we don't find the file we return an empty string
    if not post:
        post = ''

    # otherwise, we return the body of the post
    else:
        # for all image files we convert relative file paths to full file paths
        path_details = lib.misc.pathify(data_object, path_to_md_file)
        post['body'] = post['body'].replace('![](', '![]('+path_details['dir_path'])
        post = post['body']

    return post


def load_content(data_object, path_to_post, layout_name, force_listing):
    """
    Load and return post content.
    """

    loaded_content = ''
    page_object = types.SimpleNamespace()
    path_details = lib.misc.pathify(data_object, path_to_post)
    page_object.filename = path_details['html_filename']
    page_object.relative_path = path_details['relative_path']
    page_object.content = lib.reader.post(data_object, path_to_post)

    if page_object.content['published'] == 'true':

        if layout_name in data_object.config:

            loaded_content = (
                loaded_content + lib.layout.post_listing(
                    data_object, page_object, data_object.config[layout_name]
                    , force_listing=force_listing
            ))

        else:
            lib.error.output('Config has no layout with the name "'+layout_name+'"')

    return loaded_content


def jtag_load_section(data_object, jtag_name, section_name, max_posts, start_index):
    """
    Load a posts from a section.
    """

    jtag_name_main = jtag_name.split('-')[0]
    layout_id = jtag_name.split('-')[1]

    loaded_content = ''

    # define the name of the layout that we will use for presenting the content
    layout_name = 'llayout_' + layout_id

    # get all the posts for the given section
    posts = lib.builder.posts_for_section(data_object, section_name)[start_index-1:max_posts]

    # go through all posts for the given section and generate an overview
    for path_to_post in posts:

        loaded_content += load_content(data_object, path_to_post, layout_name
            , ('test' if jtag_name_main == 'LSP' else False))

    return loaded_content


def jtag_load_grid(data_object, jtag_name, section_name, max_posts, start_index):
    """
    Load section as grid.
    """

    # get all the posts for the given section
    posts = lib.builder.posts_for_section(data_object, section_name)[start_index-1:max_posts]

    # grid start
    loaded_content = data_object.config['llayout_grid_start']

    for path_to_post in posts:
        loaded_content += load_content(
              data_object, path_to_post, 'llayout_grid_content'
            , ('test' if jtag_name == 'LSG' else False)
        )

    # grid end
    loaded_content = loaded_content + data_object.config['llayout_grid_stop']

    return loaded_content


def process(data_object, md_content):
    """
    Process MD code, find JTAGs, and execute appropriate functions.
    """

    jtag_start = data_object.config['jtag_start']
    jtag_end = data_object.config['jtag_end']

    # we build a dict of replacements. if we do replacement directly, while looping, we mess up
    # things (regex searches and finds gets messed up since the text changes, and hence the
    # found indexes becomes wrong)
    replacements = {}

    # loop through all starting JTAGs
    for match in re.finditer(jtag_start, md_content):

        # make sure there is a matching end tag, otherwise break out of the loop
        matching_end_tag_index = md_content.find(jtag_end, match.end())
        if matching_end_tag_index == -1:
            break

        jtag_full = md_content[match.start():matching_end_tag_index+len(jtag_end)]
        jtag_name = get_name(data_object, jtag_full)
        jtag_size = get_size(data_object, jtag_full)
        jtag_start_index = get_start_index(jtag_full)
        jtag_value = get_value(data_object, jtag_full)

        # load external content
        if jtag_name == 'LEC':
            lib.verbose.output(
                'loading external content from "'+ jtag_value + '"', 9, data_object.verbose)
            replacements.update({jtag_full:load_external_content(jtag_value)})

        # load internal post
        elif jtag_name == 'LIP':
            lib.verbose.output(
                'loading internal post from "'+ jtag_value + '"', 9, data_object.verbose)
            replacements.update({jtag_full:jtag_load_internal_post(data_object, jtag_value)})

        # load section posts
        elif jtag_name.startswith('LSP-') or jtag_name.startswith('LSp-'):
            lib.verbose.output(
                'loading post list from section "'+ jtag_value + '"', 9, data_object.verbose)
            replacements.update(
                {jtag_full:jtag_load_section(
                      data_object
                    , jtag_name
                    , jtag_value
                    , jtag_size
                    , jtag_start_index
                    )})

        # load section as grid
        elif jtag_name in ['LSG', 'LSg']:
            lib.verbose.output(
                'loading content from section "'+ jtag_value + '"', 9, data_object.verbose)
            replacements.update(
                {jtag_full:jtag_load_grid(
                    data_object, jtag_name, jtag_value, jtag_size, jtag_start_index)})

        # HTML manipulation
        elif jtag_name in ['HTM']:
            # HTML manupilation is handled in layout.py
            pass

        # else we have an unknown JTAG
        else:
            lib.error.output('Unknown JTAG "'+jtag_name+'"')

    # loop through all the replacements, and do replace JTAGs in the markdown code with loaded
    # content. "replacements" is a dict where the key (replacement) is the full JTAG, and the value
    # (replacements[replacement]) is the loaded content.
    for replacement in replacements:
        md_content = md_content.replace(replacement, replacements[replacement])

    return md_content
