# pylint: disable=unspecified-encoding, consider-using-with
"""
20.12.12 // pqq // njs

File reader.

nifty:  `pylint --list-msgs | grep 'W0102'`
"""

from pathlib import Path
from glob import glob
import os
import cson
import lib


def file(path, mode='r'):
    """
    Read and return a file's content.
    """

    if mode == 'r':
        file_object = open(path, mode, encoding='utf-8')
    else:
        file_object = open(path, mode)

    content = file_object.read()

    file_object.close()

    return content


def config(data_object):
    """
    Read config file.
    """

    file_object = open(data_object.config_file, 'r', encoding='utf-8')
    content = cson.load(file_object)
    file_object.close()

    # add "root" to the config
    content.update({'root': os.path.dirname(data_object.config_file)})

    lib.verbose.output('Config file loaded ("'+data_object.config_file+'")', 0, data_object.verbose)

    return content


def posts(data_object):
    """
    Return a list of file names (of type PosixPath) to markdown posts.
    """

    # Path().rglob() is not following symlinks, however, it is following dotted files which glob() is not
    # https://stackoverflow.com/questions/49047402/python-glob-include-hidden-files-and-folders
    content_list_1_tmp = list(Path(data_object.config['root'] + lib.constants.DIR_SECTIONS + '/').rglob("*.[mM][dD]"))
    content_list_1 = list()

    # convert all entries to string, as string value is needed for symlinks to function properly
    for entry in content_list_1_tmp:
        content_list_1.append(str(entry))

    # glob() is following symlinks.
    content_list_2 = list(glob(data_object.config['root'] + lib.constants.DIR_SECTIONS + '/**/*.[mM][dD]', recursive=True))

    # merge the two lists
    content_list = content_list_1 + content_list_2

    # remove dupes | https://www.geeksforgeeks.org/python-ways-to-remove-duplicates-from-list/
    content_list = [*set(content_list)]

    # build a list of indexes to posts that shall not be processed
    indexes_to_remove = []
    for idx in range(0, len(content_list), 1):

        # pathyfiy the string
        path = Path(content_list[idx])

        relative_path = lib.misc.pathify(data_object, path.resolve())['relative_path']

        if (
               relative_path.startswith('-') # check if the first folder starts with a dash
            or '/-' in relative_path         # or any other subfolder, or post, starts with a dash
            or '/-' in str(path)             # in case of symlinks path needs to be checked as well, as path will be the symlink name and relative_path will be the target
        ):
            indexes_to_remove.append(idx)

    # for avoiding the indexes becoming invalid we have to delete in a reversed manner. for example,
    # if you delete index 1 then previous index 2 becomes index 1 after deletion.
    for idx in reversed(indexes_to_remove):
        del content_list[idx]

    lib.verbose.output(str(len(content_list)) + ' posts loaded', 0, data_object.verbose)

    return sorted(content_list)


def post(date_object, path):
    """
    Read single post and return its content as dictionary.
    """

    # read content from os
    try:
        file_object = open(path, 'r', encoding='utf-8')
        content = file_object.read()
        file_object.close()

    except FileNotFoundError:
        lib.error.output('reader.post(): File not found: "' + path + '"')
        return None

    # a content file contains of three parts, divided by three tilde characters
    parts = content.split('~~~\n')

    # if we have less than 3 parts, it's an error (having more is ok)
    if len(parts) < 3:
        lib.error.output('reader.post(): Unknown content formatting: "' + str(path) + '"')
        return None

    # build the return dictionary
    cont_dict = {}

    # part one contains of details
    if 'title:' in parts[0]:
        cont_dict.update({'title': parts[0].split('title:')[1].split('\n')[0].strip() })
    else:
        cont_dict.update({'title': '.....'})
    if 'date:' in parts[0]:
        cont_dict.update({'date': parts[0].split('date:')[1].split('\n')[0].strip() })
    else:
        cont_dict.update({'date': date_object.config['post_no_date_found']})
    if 'published:' in parts[0]:
        cont_dict.update({'published': parts[0].split('published:')[1].split('\n')[0].strip() })
    else:
        cont_dict.update({'published': 'true'})
    if 'comments:' in parts[0]:
        cont_dict.update({'comments': parts[0].split('comments:')[1].split('\n')[0].strip() })
    else:
        cont_dict.update({'comments': 'false'})
    if 'image:' in parts[0]:
        cont_dict.update({'image': parts[0].split('image:')[1].split('\n')[0].strip() })
    else:
        cont_dict.update({'image': None})

    # part two contains the ingress
    cont_dict.update({'ingress': parts[1].replace('\n', '')})

    # part three contains the body
    cont_dict.update({'body': parts[2]})

    return cont_dict


def exported_html(data_object):
    """
    Read and return all the HTML files in the export directory.
    """

    content_list = list(
        Path(data_object.config['root'] + data_object.config['export_dir'])
            .rglob("*.[Hh][Tt][Mm][Ll]", )
    )

    lib.verbose.output(str(len(content_list)) + ' exported html files read', 4, data_object.verbose)

    return sorted(content_list)
