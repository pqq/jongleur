"""
20.12.25 // pqq // njs

An overview of all the constants used in Jongleur.
"""

DIR_SECTIONS = '/content/sections'
FILE_INDEXHTML = '/layout/index.html'
FILE_JSONFEED = '/layout/feed.json'
FILE_ROBOTSTXT = '/layout/robots.txt'
FILE_RSSFEEDXML = '/layout/rssfeed.xml'
FILE_SITEMAPXML = '/layout/sitemap.xml'
FILE_STYLESCSS = '/layout/styles.css'
FILE_HOME_OUT = 'index.html'
FILE_HOME_IN = '/content/home/_home.md'
DIR_HOME = '/content/home'
FILE_OVERVIEW_CONTENT = 'index.markdown'
