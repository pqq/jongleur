"""
20.12.31 // pqq // njs

Functions that handle the layout of the HTML page are found here.
"""

import datetime
import glob
import os
import shutil
import types
from bs4 import BeautifulSoup as BSHTML
import lib


def __link_replacer(data_object, link):
    """ Given a link, replace parts of it. """

    # replace string in internal links
    if data_object.config['internal_link_replacer']:
        lines = data_object.config['internal_link_replacer'].split('\n')
        for line in lines:
            before_string, after_string = line.split('>>')
            before_string = before_string.rstrip().lstrip()
            after_string = after_string.rstrip().lstrip()
            link = link.replace(before_string, after_string)

    return link


def __image_process(data_object, page_object):
    """
    Given page_object.content['image'], return the correct image data.
    """
    return_data = ''

    # if we are using an external image (as an url to the image)
    if page_object.content['image'].startswith('http'):
        return_data = page_object.content['image']

    # if we are using a local image
    else:

        image_path = ''

        # if we have only a filename, with no path
        if not '/' in page_object.content['image']:
            relative_path = page_object.relative_path.rpartition('/')[0]
            image_path = relative_path + '/' + page_object.content['image']

        # else if we have a relative path to an image
        else:
            image_path = page_object.content['image']

        image_path = lib.misc.to_full_path(data_object, image_path)
        image_path = __link_replacer(data_object, image_path)

        return_data = lib.image.base64_encode(data_object, image_path)

    return return_data


def deploy(data_object):
    """
    Deploy/copy the layout files to the export directory.
    """

    # robots.txt
    file_to_copy = data_object.config['root'] + lib.constants.FILE_ROBOTSTXT
    lib.misc.check_file_existence(file_to_copy, True)
    shutil.copy(file_to_copy, data_object.config['root']+data_object.config['export_dir'])

    # styles.css
    file_to_copy = data_object.config['root'] + lib.constants.FILE_STYLESCSS
    lib.misc.check_file_existence(file_to_copy, True)
    shutil.copy(file_to_copy, data_object.config['root']+data_object.config['export_dir'])

    # assets/
    # https://pynative.com/python-copy-files-and-directories/
    source_dir = data_object.config['root']+'/content/assets'
    target_dir = data_object.config['root']+data_object.config['export_dir']+'/assets/'
    shutil.copytree(
          source_dir, target_dir
        , symlinks=False, ignore=None, copy_function=shutil.copy2
        , ignore_dangling_symlinks=False, dirs_exist_ok=True
    )

    # favicon/
    # since shutil.copytree fails with 'File exists' if dir is not empty, we use loop and copy():
    target_dir = data_object.config['root']+data_object.config['export_dir']+'/favicon/'
    if not os.path.exists(os.path.dirname(target_dir)):
        os.makedirs(os.path.dirname(target_dir))
    for filename in glob.glob(os.path.join(data_object.config['root']+'/layout/favicon', '*.*')):
        shutil.copy(filename, target_dir)

    # fonts/
    target_dir = data_object.config['root']+data_object.config['export_dir']+'/fonts/'
    if not os.path.exists(os.path.dirname(target_dir)):
        os.makedirs(os.path.dirname(target_dir))
    for filename in glob.glob(os.path.join(data_object.config['root']+'/layout/fonts', '*.*')):
        shutil.copy(filename, target_dir)

    lib.verbose.output('Layout files deployed to '
        + data_object.config['root'] + data_object.config['export_dir'], 0, data_object.verbose)


def load(data_object):
    """
    Load and return the main layout file, 'index.html'.
    """

    file_to_load = data_object.config['root'] + lib.constants.FILE_INDEXHTML

    lib.misc.check_file_existence(file_to_load, True)

    lib.verbose.output('Layout file loaded ("'+file_to_load+'")', 0, data_object.verbose)

    return lib.reader.file(file_to_load)


def sections_as_html_links(data_object, html_object):
    """
    Loop through the sections and build HTML links out of them.
    """

    html_code = ''
    section_links = {}

    for section in data_object.sections:
        section_raw = section

        # ignore creating a menu item for sections starting with a dot (hidden entries)
        if section.startswith('.'):
            continue

        # ignore home link / button on home page, if specified in config
        if (
                section == html_object.active_section
            and section == data_object.config['header_home']
            and data_object.config['header_home_hide']
            ):
            continue

        # it's recommended that the link for the home section is index.html, as that normally is
        # the default page to load for a web server
        if section == data_object.config['header_home']:
            overview_page_name = lib.constants.FILE_HOME_OUT.split('.', maxsplit=1)[0]
        else:
            overview_page_name = section

        # make the active section stand out, for ease of navigation
        if section == html_object.active_section:
            css_class = html_object.active_section_css
        else:
            css_class = html_object.inactive_section_css

        # append menu style to header/menu links
        if data_object.config['header_menu_style'] == 'upper':
            section = section.upper()
        elif data_object.config['header_menu_style'] == 'title':
            section = section.title()
        elif data_object.config['header_menu_style'] == 'capitalize':
            section = section.capitalize()

        # replace underscore with space, for the title
        section = section.replace('_', ' ')

        # for all header elements we do add equal padding on each side for symmetri
        section_links[section_raw] = (
              html_object.pre_code
            + html_object.link_code.replace('[CSS_CLASS]', css_class)
                .replace('[URL]', overview_page_name+'.html')
                .replace('[TITLE]', section)
        )

    if 'header_menu_order' in data_object.config and data_object.config['header_menu_order']:
        for menu in data_object.config['header_menu_order']:
            if not menu in section_links:
                lib.error.output(f'menu element {menu} exists in header_menu_order value in config, but is not a valid section folder')
            else:
                html_code += section_links[menu]
    else:
        for link in section_links:
            html_code += section_links[link]

    return html_code


def menu_by_type(data_object, active_section, mobile=False):
    """
    Build and return the menu as HTML, mobile or non-mobile/desktop as type.
    """

    html_code = ''

    # create html_object to hold key data
    #
    # key data:
    # -----------------------------
    # html_object.active_section
    # html_object.active_section_css
    # html_object.inactive_section_css
    # html_object.pre_code
    # html_object.link_code
    # html_object.post_code
    # -----------------------------
    html_object = types.SimpleNamespace()
    html_object.active_section = active_section

    # make the active section stand out, for ease of navigation
    if mobile:
        html_object.active_section_css = 'header_link_active_mobile'
        html_object.inactive_section_css = 'header_link_inactive_mobile'
    else:
        html_object.active_section_css = 'header_link_active'
        html_object.inactive_section_css = 'header_link_inactive'

    # set the code for each section item
    if mobile:
        html_object.pre_code = '<li>'
        html_object.link_code = '<a class="[CSS_CLASS]" href="[URL]" target="_top">[TITLE]</a>'
        html_object.post_code = '</li>'
    else:
        # equal padding on each side for symmetri
        html_object.pre_code = '<span class="header_link_spacing"></span>'
        html_object.link_code = '<a class="[CSS_CLASS]" href="[URL]" target="_top">[TITLE]</a>'
        html_object.post_code = '<span class="header_link_spacing"></span>'

    # add prefix code
    if data_object.config['header_menu_prefix'] != '' and not mobile:
        html_code += data_object.config['header_menu_prefix']
    elif data_object.config['header_menu_prefix_mob'] != '' and mobile:
        html_code += data_object.config['header_menu_prefix_mob']

    # first code for the menu
    if mobile:
        html_code += (
            '<span id="mob-menu"><ul><input type="checkbox" id="collapse" checked/>'
            + '<label for="collapse"></label>')

    # convert section into html links
    html_code += sections_as_html_links(data_object, html_object)

    # last code for the menu
    if mobile:
        html_code += '</ul></span>'

    # add postfix code
    if data_object.config['header_menu_postfix'] != '' and not mobile:
        html_code += data_object.config['header_menu_postfix']
    elif data_object.config['header_menu_postfix_mob'] != '' and mobile:
        html_code += data_object.config['header_menu_postfix_mob']

    return html_code


def menu(data_object, active_section):
    """
    Build and return the menu as dictionary.
    """

    menu_dict = {}
    menu_dict.update({'desktop': menu_by_type(data_object, active_section, mobile=False)})
    menu_dict.update({'mobile': menu_by_type(data_object, active_section, mobile=True)})

    return menu_dict


def link_target_handler(data_object, html_code):
    """
    Handle link targets.

    Supported tags:
    [c:]() - content delivery network link - open via content delivery network service
    [e:]() - external links - open in new window
    [i:]() - internal link, to an existing post, open in same tab
    [ei:]() - internal link, open in new window

    Not (yet) supported tags:
    [r:]() - raw mode link - open file as it is, without any additional layout
    """

    soup = BSHTML(html_code, features='html5lib')

    links = soup.findAll('a')
    external_link_icon = ('<span class="external_link_icon">'
        + data_object.config['external_link_icon'] + '</span>')

    # html to md is converting special characters in a way that might differ to BSHTML
    # to avoid mismatch when trying to replace links, we use html codes for a few special
    # characters that might be used in the link names / strings
    # related issue: https://gitlab.com/pqq/jongleur/-/issues/22
    html_code = html_code.replace('’', '&rsquo;').replace('“', '&ldquo;').replace('”', '&rdquo;')

    for link in links:

        # skip empty links
        # example:
        #   <a href="" id="email" target="_blank"></a>
        if not link.string and len(link.contents) == 0:
            continue

        # -----
        # handle image links
        # -----

        if '<img' in str(link):

            # skip image links without link target tags ("e:", "i:" etc)
            if len(link.contents) == 1:
                continue

            if link.contents[0] == 'c:':
                link_orig = str(link).split('c:', maxsplit=1)[0]
                url_part = link_orig.split('"')[1]

                link_new = (
                      '<a href="' + data_object.config['cdn'].replace('[PATH]', url_part)
                    + '" target="_blank">'
                )
                html_code = html_code.replace(link_orig + 'c:', link_new)
                continue

            if link.contents[0] == 'e:':
                link_orig = str(link).split('e:', maxsplit=1)[0]
                link_new = link_orig.replace('">', '" target="_blank">')
                html_code = html_code.replace(link_orig + 'e:', link_new)
                continue

            if link.contents[0] in ('i:', 'ie:', 'ei:'):
                tag = link.contents[0]
                target = '_top'
                if tag in ('ie:', 'ei:'):
                    target = '_blank'
                link_orig = str(link).split(tag, maxsplit=1)[0]
                link_new = (
                    lib.misc.pathify(data_object, link_orig)['html_filename'].replace('">'
                        , '" target="'+target+'">'))
                html_code = html_code.replace(link_orig + tag, link_new)
                continue

            # for all other cases, we continue...
            continue

        # -----
        # handle text links
        # -----
        str_link = str(link).replace('’', '&rsquo;').replace('“', '&ldquo;').replace('”', '&rdquo;')

        # if the link name contains html code, link.string becomes NoneType. in these cases we can look at the first element
        # of link.contents instead. as this is where the special tag will be
        if not link.string:
            link.string = link.contents[0]

        # c: content delivery network links
        if link.string.startswith('c:'):
            html_code = html_code.replace(
                  str_link
                , '<a href="' + data_object.config['cdn'].replace('[PATH]', link['href'])
                + '" target="_blank">' + link.string.replace('c:', '') + external_link_icon + '</a>'
            )

        # e: external links
        elif link.string.startswith('e:'):
            html_code = html_code.replace(
                  str_link
                , '<a href="' + link['href'] + '" target="_blank">'
                + link.string.replace('e:', '') + external_link_icon + '</a>'
            )

        # i: & ie: / ei: internal links
        elif (
               link.string.startswith('i:')
            or link.string.startswith('ie:')
            or link.string.startswith('ei:')
            or link.string.startswith('_i:')
            or link.string.startswith('_ie:')
            or link.string.startswith('_ei:')
        ):
            # replace string in internal links
            link['href'] = __link_replacer(data_object, link['href'])

            # if we have an starting underscore, the file existence check will be ignored
            if not link.string.startswith('_'):
                lib.misc.check_file_existence(
                    data_object.config['root'] + lib.constants.DIR_SECTIONS +  '/' + link['href']
                    , False)
            else:
                link.string = link.string[1:]

            if link.string.startswith('i:'):
                target = '_top'
                external_link_icon_ = ''
            elif link.string.startswith('ie:') or link.string.startswith('ei:'):
                target = '_blank'
                external_link_icon_ = external_link_icon

            html_code = html_code.replace(
                  str_link
                , '<a href="' + lib.misc.pathify(data_object, link['href'])['html_filename'] + '" target="'+target+'">'
                + link.string.replace('ie:', '').replace('ei:', '').replace('i:', '')
                + external_link_icon_ + '</a>'
            )

    return html_code


def footer(data_object):
    """
    Return properly formatted footer code.
    """

    today = datetime.datetime.now()
    foot = (data_object.config['footer']
        .replace('[BUILDTIME]', today.strftime(data_object.config['build_date_format'])))

    return foot


def row(layout, tag_to_replace, content, css_class, post_tag, cols='1-10-1', bg=None):
    """
    Return properly formatted code for a <div> row.
    """

    # skip the columns, and only use the row
    if cols == 'skip':
        if bg:
            bg = f'background-color: {bg};'
        return layout.replace(tag_to_replace
                ,f'<div class="row" style="{bg}">'
                +       content
                + '</div>'
                + post_tag
            )

    # columns data given
    else:
        return layout.replace(tag_to_replace
                , '<div class="row">'
                +   f'<div class="col-{cols.split("-")[0]}">&nbsp;</div>'
                +   f'<div class="col-{cols.split("-")[1]} {css_class}">'
                +       content
                +   '</div>'
                +   f'<div class="col-{cols.split("-")[2]}">&nbsp;</div>'
                + '</div>'
                + post_tag
            )


def htm_parts_parser(data_object, layout, tag_to_replace, content, css_class, post_tag):
    """ Handle HTM content parts. """

    parts = content.split(f'{data_object.config["jtag_start"]}HTM=')
    no_parts = len(parts)

    if no_parts == 1:
        layout = row(layout, tag_to_replace, content, css_class, post_tag)

    else:
        for part in parts:
            content_new = part

            new_post_tag = post_tag
            if no_parts > 1:
                new_post_tag = tag_to_replace

            cols    = '1-10-1'
            bg      = None
            row_op  = None

            htm_settings = part.split('}}')[0]
            for htm_set in htm_settings.split(':'):

                if htm_set == 'row':
                    row_op = True
                elif htm_set.startswith('cols='):
                    cols = htm_set.split('=')[1]
                elif htm_set.startswith('bg='):
                    bg = htm_set.split('=')[1]

            if row_op:
                content_new = content_new.split('}}')[1].replace('<br />', '')

            layout = row(layout, tag_to_replace, content_new, css_class, new_post_tag, cols, bg)

            no_parts -= 1

    return layout


def page(data_object, page_object, page_type):
    """
    Set up the HTML layout for a page.

    page_type:
        home        - home page
        overview    - a section's overview page, of related posts
        post        - a post page
    """

    layout = data_object.layout

    # ---
    # set heading information

    ## description
    description = data_object.config['site_description']
    if isinstance(page_object.content, dict) and 'ingress' in page_object.content:
        description = page_object.content['ingress']
    layout = layout.replace('[DESCRIPTION]', description)

    ## image
    image = f"{data_object.config['base_url']}{data_object.config['site_image']}"
    if isinstance(page_object.content, dict) and 'image' in page_object.content:
        if page_object.content['image'] is not None and page_object.content['image'] != '':
            image = __image_process(data_object, page_object)
    layout = layout.replace('[IMAGE]', image)

    ## url
    layout = layout.replace('[URL]', f"{data_object.config['base_url']}/{page_object.filename}")

    ## title
    if page_type == 'home':
        layout = layout.replace('[TITLE]'
            , data_object.config['site_title_prefix']
                + data_object.config['site_title_home'] + data_object.config['site_title_postfix'])

    elif page_type == 'overview':
        layout = layout.replace('[TITLE]'
            , data_object.config['site_title_prefix']
                + page_object.section + data_object.config['site_title_postfix'])

    elif page_type == 'post':
        layout = layout.replace('[TITLE]', data_object.config['site_title_prefix'] + \
             page_object.content['title'] + data_object.config['site_title_postfix'])

    # set the (web) theme colour
    layout = layout.replace('[WEB_THEME_COLOUR]', data_object.config['web_theme_colour'])

    # set the code additions
    layout = layout.replace('[CODE_ADDITION_HEAD]', data_object.config['code_addition_head'])
    layout = layout.replace('[CODE_ADDITION_BODY]', data_object.config['code_addition_body'])

    # set banner
    if data_object.config['banner_enabled']  == True:
        layout = layout.replace('[BANNER]'
                , f'<img class="banner" src="{data_object.config['banner_image']}" />'
        )
    else:
        layout = layout.replace('[BANNER]', '')

    # set the header (top menu)
    layout = layout.replace('[HEADER]'
            , data_object.config['header_first']
            + '<div class="header hide_on_mobile">'
            + page_object.menu['desktop']
            + '</div>'
            + '<span class="display_on_mobile">'
            + page_object.menu['mobile']
            + '</span>'
            + data_object.config['header_last']
        )

    # set the main content
    if page_type == 'home':
        if len(page_object.header_sub) > 0:
            layout = row(layout, '[HEADER_SUB]', page_object.header_sub, 'header_sub', '')
        else:
            layout = layout.replace('[HEADER_SUB]', '')

        layout = htm_parts_parser(data_object, layout, '[MAIN]', page_object.content, 'post_main', '')

    elif page_type == 'overview':
        layout = layout.replace('[HEADER_SUB]', '')
        layout = row(layout, '[MAIN]', page_object.content, 'post_main', '[POST]')

        for post in page_object.posts:
            if post:
                layout = row(layout, '[POST]', post, 'post_listing', '[POST]')

        layout = layout.replace('[POST]', '')

    elif page_type == 'post':
        layout = layout.replace('[HEADER_SUB]', '')
        layout = row(layout, '[MAIN]', page_object.content['title'], 'post_title', '[INGRESS]')
        layout = row(layout, '[INGRESS]', page_object.content['ingress'], 'post_ingress', '[BODY]')
        layout = htm_parts_parser(data_object, layout, '[BODY]', lib.markd.htmlify(data_object, page_object.dir_path, page_object.content['body']), 'post_main', '[TIMESTAMP]')
        layout = row(layout, '[TIMESTAMP]', page_object.content['date'], 'timestamp', '')

        # include commenting script if enabled on post
        if page_object.content['comments'] == 'true':
            layout = row(
                layout, '[COMMENTS]'
                    , data_object.config['commenting']
                        .replace('[COMMENT_SECTION_ID]'
                            , page_object.filename.replace('.', '').replace('_', '')
                            )
                    , 'comments', '')

    # remove unused comments tags, if commenting is disabled
    layout = layout.replace('<div class="comments row">[COMMENTS]</div>', '')

    # set the footer content
    layout = layout.replace('[FOOTER]', footer(data_object))

    return layout


def post_listing(data_object, page_object, layout, force_listing=False):
    """
    Set up the HTML layout for a post listing, used on overview pages.
    """

    # we shall not create any post listing for posts, or posts in directories, names that starts
    # with a dot as that means the post shall be hidden.
    if (
        not force_listing and (
               page_object.relative_path.startswith('.') # check if first folder starts with a dot
            or '/.' in page_object.relative_path         # or any other subfolder, or post...
        )
    ):
        return ''

    layout = (
        layout
            .replace('[DATE]', page_object.content['date'])
            .replace('[URL]', page_object.filename)
            .replace('[TITLE]', page_object.content['title'].replace('"','&quot;'))
            .replace('[INGRESS]', page_object.content['ingress'].replace('"','&quot;'))
            .replace('[ID]', page_object.content['title'].replace(' ', '').replace('(', '')
                .replace(')', '').replace('`', '').replace('\'', '').replace(',', ''))
    )

    if page_object.content['image']:
        layout = layout.replace('[IMAGE]', __image_process(data_object, page_object))

    return layout
