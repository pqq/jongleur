"""
20.12.08 // pqq // njs

Error handling.
"""

import io
import sys
import traceback
from contextlib import redirect_stdout


def output(message, do_exit=False):
    """
    Print error message.
    """

    print('\nERROR: ' + message + '\n')
    if do_exit:
        sys.exit()


def test_output():
    """
    Test
    """
    try:
        with io.StringIO() as buf, redirect_stdout(buf):
            print('fbk')
            output_ = buf.getvalue()
    except ValueError as value_error:
        raise ValueError(
            'error.test_output()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------') from value_error
    if not 'fbk' in output_:
        raise ValueError('error.test_output()', 'f to the b to the k is missing')
