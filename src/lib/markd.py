"""
20.12.31 // pqq // njs

Markdown related functions.
"""

import markdown
import lib


def md2html(markd):
    """
    Convert markdown into HTML.

    Markdown extensions: https://python-markdown.github.io/extensions/
    """

    html = markdown.markdown(
        markd, extensions=[
              'markdown.extensions.abbr'
            , 'markdown.extensions.attr_list'
            , 'markdown.extensions.def_list'
            , 'markdown.extensions.fenced_code'
            , 'markdown.extensions.footnotes'
            , 'markdown.extensions.md_in_html'
            , 'markdown.extensions.nl2br'
            , 'markdown.extensions.sane_lists'
            , 'markdown.extensions.smarty'
            , 'markdown.extensions.tables'
            , 'markdown.extensions.toc'
            ])
    return html


def htmlify(data_object, directory, md_content):
    """
    HTMLify Jongleur's markdown content.
    """

    return lib.layout.link_target_handler (
              data_object
            , lib.image.process (
                      data_object
                    , directory
                    , md2html (
                        lib.jtag.process (
                              data_object
                            , md_content
                        )
                    )
                )
        )
