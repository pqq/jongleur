"""
21.03.16 // pqq // njs

Feed generator, following the standard defined at:
https://www.rssboard.org/rss-specification
https://jsonfeed.org/

https://json-feed-validator.herokuapp.com/validate
"""

import datetime
import lib


def load(data_object):
    """
    Load and return the template feed files.
    """

    template_files = {}

    template_files.update(
        {'rss_template_file': data_object.config['root'] + lib.constants.FILE_RSSFEEDXML})
    template_files.update(
        {'json_template_file': data_object.config['root'] + lib.constants.FILE_JSONFEED})

    lib.misc.check_file_existence(template_files['rss_template_file'], True)
    lib.misc.check_file_existence(template_files['json_template_file'], True)

    lib.verbose.output(
        'RSS file loaded ("' + template_files['rss_template_file'] + '")', 4, data_object.verbose)
    lib.verbose.output(
        'JSON file loaded ("' + template_files['json_template_file'] + '")', 4, data_object.verbose)

    file_content = {}
    file_content.update({'rss': lib.reader.file(template_files['rss_template_file'])})
    file_content.update({'json': lib.reader.file(template_files['json_template_file'])})

    return file_content


def generate(data_object):
    """
    Generate the feed, parsing posts.
    """

    lib.verbose.output('Generating feed code from posts', 4, data_object.verbose)

    items = {}
    rss_items = ''
    json_items = ''

    for post in data_object.content:

        post_path_details = lib.misc.pathify(data_object, post)
        post_content = lib.reader.post(data_object, post)

        date_time_str = post_content['date']
        try:
            # https://stackabuse.com/converting-strings-to-datetime-in-python/
            date_time_obj = (
                datetime.datetime.strptime(date_time_str, data_object.config['post_date_format']))

        except ValueError:
            lib.error.output(
                'feed.generate(): date format: "' + date_time_str + '" in "'
                    + str(post) + '" does not match config\'s "post_date_format": "'
                    + data_object.config['post_date_format'] +'"'
            )
            continue

        rss_items += '<item>\n'
        rss_items += '  <title>' + lib.www.html_encode(post_content['title']) + '</title>\n'
#        rss_items += '  <title>' + post_content['title'] + '</title>\n'
        rss_items +=('  <description>'
            + lib.www.html_encode(post_content['ingress']) + '</description>\n')
        rss_items += '  <link>' + post_path_details['url'] + '</link>\n'
        rss_items += '  <guid>' + post_path_details['url'] + '</guid>\n'
        rss_items += '  <pubDate>' + lib.www.to_rfc2822(date_time_obj) + '</pubDate>\n'
        rss_items += '</item>\n'

        json_items += '{\n'
        json_items += '  "id": "' + post_path_details['url'] + '",\n'
        json_items += '  "url": "' + post_path_details['url'] + '",\n'
        json_items += '  "title": "' + lib.www.html_encode(post_content['title']) + '",\n'
        json_items += '  "content_html": "' + lib.www.html_encode(post_content['ingress']) + '",\n'
        json_items += '  "date_published": "' + lib.www.to_rfc3339(date_time_obj) + '"\n'
        json_items += '}\n'
        json_items += ','

    json_items = lib.misc.rreplace(json_items, ',', '', 1)

    items.update({'rss': rss_items})
    items.update({'json': json_items})

    return items


def write(data_object, feed_templates, items):
    """
    Write the rss feed to the export directory.
    """

    for template in feed_templates:

        if template == 'rss':
            feed_file_name = 'rssfeed.xml'
        elif template == 'json':
            feed_file_name = 'feed.json'

        lib.verbose.output('Creating ' + feed_file_name, 4, data_object.verbose)

        feed_templates[template] = (
            feed_templates[template]
                .replace('[FEED_TITLE]', data_object.config['feed_title'])
                .replace('[BASE_URL]', data_object.config['base_url'])
                .replace('[FEED_DESCRIPTION]', data_object.config['feed_description'])
                .replace('[FEED_FILE_NAME]', feed_file_name)
                .replace('[LAST_BUILD_DATE]', lib.www.to_rfc2822())
                .replace('[ITEMS]', items[template])
            )

        with open(data_object.config['root'] + data_object.config['export_dir'] + '/' + feed_file_name, 'w'
            , encoding='utf-8') as feed_file:
            feed_file.write(feed_templates[template])


def create(data_object):
    """
    Create feeds.
    """

    lib.verbose.output('Creating feeds:', 0, data_object.verbose)
    feed_templates = load(data_object)
    items = generate(data_object)
    write(data_object, feed_templates, items)
