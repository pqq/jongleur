"""
20.12.31 // pqq // njs

Functions related WWW (web) requests and operations.
"""

import datetime
import email.utils
import ssl
import time
import urllib
import lib


def fetch(url):
    """
    Fetch content, given a URL.

    User agent is sometimes needed for avoiding HTTP 403 errors.

    Pages where you can view your own user-agent:
    - https://wtools.io/check-my-user-agent
    - https://manytools.org/http-html-text/user-agent-string/
    """

    # https://stackoverflow.com/questions/36600583/python-3-urllib-ignore-ssl-certificate-verification
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    try:
        req = urllib.request.Request(
              url
            , data=None
            , headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0'}
        )

        with urllib.request.urlopen(req, context=ctx) as file_object:
            remote_content = file_object.read().decode('utf-8')

        return remote_content

    except urllib.error.HTTPError as http_error:
        lib.error.output('Failed loading <'+url+'> due to HTTP error <'+str(http_error.code)+' - '+http_error.reason+'>')
        raise


def html_encode(html):
    """
    HTML encode the given string.

    https://www.asciitable.com/
    https://www.w3schools.com/tags/ref_urlencode.asp
    """

    return (
        html.replace('&', '&#38;').replace(' ', '&#32;').replace('"', '&#34;')
            .replace('\'', '&#39;').replace('<', '&#60;').replace('>', '&#62;')
            .replace('’', '&#8217;')
    )


def to_rfc2822(date_time_obj=None):
    """
    Returns a RFC 2822 formatted date string.

    https://tools.ietf.org/html/rfc2822.html
    https://stackoverflow.com/questions/3453177/convert-python-datetime-to-rfc-2822
    """

    if date_time_obj is None:
        date_time_obj = datetime.datetime.now()

    nowtuple = date_time_obj.timetuple()
    nowtimestamp = time.mktime(nowtuple)

    return email.utils.formatdate(nowtimestamp)


def to_rfc3339(date_time_obj=None):
    """
    Returns a RFC 3339 formatted date string.

    https://tools.ietf.org/html/rfc3339
    https://stackoverflow.com/questions/2150739/iso-time-iso-8601-in-python
    """

    if date_time_obj is None:
        date_time_obj = datetime.datetime.now()

    return date_time_obj.isoformat()
