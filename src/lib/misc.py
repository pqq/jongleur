"""
20.12.31 // pqq // njs

Miscellaneous functions that doesn't fit elsewhere. This is like a library within the library.
"""

import os
import shutil
import lib


def empty_export_dir(data_object):
    """
    Empty Jongleur's export directory. Contains some extra redundant checks, to making it harder to delete wrong content.
    """

    def check_and_delete(dir_or_file, full_path):
        """
        An extra check, to prevent deleting the wrong content.
        """

        if not ('export' and 'jongleur') in full_path:
            lib.error.output('The path "' + full_path + '" does not contain needed keywords.'
            + '" To prevent potential damage, no deletion will be done.', True)

        if dir_or_file == 'file':
            os.remove(full_path)
        elif dir_or_file == 'dir':
            shutil.rmtree(full_path)


    if not 'export' in data_object.config['export_dir']:
        lib.error.output('The export directory "' + data_object.config['export_dir']
        + '" does not contain the mandatory phrase "export". Jongleur is stopping to prevent '
        + 'potentially messing with a wrong directory. Please, update the name of your export '
        + ' directory.', True)

    path_to_export_dir = data_object.config['root']+data_object.config['export_dir']

    if not 'jongleur' in path_to_export_dir:
        lib.error.output('The full path to the export directory "' + path_to_export_dir
        + '" does not contain the mandatory phrase "jongleur". Jongleur is stopping to prevent '
        + 'potentially messing with a wrong directory. Please, update the path to your project'
        + ' directory.', True)

    # recursively delete files on the os
    for root, dirs, files in os.walk(path_to_export_dir):

        # delete all sub directories
        for directory in dirs:
            check_and_delete('dir', os.path.join(root, directory))

        # delete all files that were not in a sub directory
        for file in files:
            check_and_delete('file', os.path.join(root, file))


def pathify(data_object, path_to_file):
    """
    Given a path to a file on the OS, return derived details.
    """

    details = {}

    #  /run/media/pqq/dataDrive/cloud-drive/jongleur/content/sections/projects/p3/index.md
    details.update({'full_path': str(path_to_file)})

    #  /run/media/pqq/dataDrive/cloud-drive/jongleur/content/sections/projects/p3/
    details.update({'dir_path': str(path_to_file).rsplit('/', 1)[0] + '/'})

    #  projects/p3/index.md
    details.update({'relative_path':
        str(path_to_file).replace(data_object.config['root'] + lib.constants.DIR_SECTIONS
        + '/', '')})

    #  projectsp3index.html
    details.update(
        {'html_filename': details['relative_path'].replace('/', '').replace('.md', '.html')})

    #  https://example.com/projectsp3index.html
    details.update({'url': data_object.config['base_url'] + '/' + details['html_filename']})

    return details


def to_full_path(data_object, path_to_file):
    """
    Given a full or relative path, or just a file name, always return the full path.
    """

    post_path = ''

    if lib.constants.DIR_SECTIONS not in path_to_file:
        post_path = lib.constants.DIR_SECTIONS + '/'

    if data_object.config['root'] not in path_to_file:
        post_path = data_object.config['root'] + '/' + post_path

    # we remove recurring slashes from the paths (not needed, but looks better)
    return_path = (post_path + path_to_file).replace('///', '/').replace('//', '/')

    return return_path


def check_file_existence(path_to_file, do_exit):
    """
    Check if file is found on the OS.
    """

    if not os.path.isfile(path_to_file):
        lib.error.output('File not found: "' + path_to_file + '"', do_exit)


def rreplace(in_string, old, new, occurrence):
    """
    Right replace, or replace starting from the right/end of a string.

    https://stackoverflow.com/questions/2556108/
        rreplace-how-to-replace-the-last-occurrence-of-an-expression-in-a-string
    """
    result_list = in_string.rsplit(old, occurrence)
    return new.join(result_list)
