"""
20.12.13 // pqq // njs

Output in verbose mode.
"""

import os
import sys
import traceback


def output(message, leading_spaces=0, verbose=False):
    """
    Output messages to the CLI.
    """

    # add leading spaces to the message string
    # https://www.datasciencemadesimple.com/add-spaces-in-python/
    message_length = len(message) + leading_spaces
    message = message.rjust(message_length)

    if verbose:
        print('# ' + message)


def test_output():
    """
    Test.
    """
    try:
        sys.stdout = open(os.devnull, 'w')
        output('say o nara')
    except ValueError as value_error:
        raise ValueError(
            'verbose.test_output()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------') from value_error
    finally:
        sys.stdout = sys.__stdout__
