# pylint: disable=unused-import,import-outside-toplevel
"""
20.12.11 // pqq // njs

Testing imports of third party python modules.
"""


def do_import():
    """
    Test imports.
    """

    try:
        import bs4
    except ImportError as import_error:
        import traceback
        raise ValueError(
            'test_imports.do_import()',
            'feedparser import error \n------------------\n' +
            traceback.format_exc() + '------------------') from import_error

    try:
        import markdown
    except ImportError as import_error:
        import traceback
        raise ValueError(
            'test_imports.do_import()',
            'markdown import error \n------------------\n' +
            traceback.format_exc() + '------------------') from import_error

    try:
        import PIL
    except ImportError as import_error:
        import traceback
        raise ValueError(
            'test_imports.do_import()',
            'PIL import error \n------------------\n' +
            traceback.format_exc() + '------------------') from import_error

    try:
        import cson
    except ImportError as import_error:
        import traceback
        raise ValueError(
            'test_imports.do_import()',
            'cson import error \n------------------\n' +
            traceback.format_exc() + '------------------') from import_error
