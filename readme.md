# Jongleur; a Python framework for building websites

Jongleur generates static HTML pages from markdown pages.

#### Build status

[![Build status is not (yet) implemented](https://gitlab.com/pqq/jongleur/badges/master/build.svg)](https://gitlab.com/pqq/jongleur/pipelines)

# Sites built by Jongleur

Below is our example site, plus a few more sites, that all have been built using Jongleur.

- [Jongleur Example Site](https://pqq.gitlab.io/jongleur.example/)
- [antijanteboka.com](https://antijanteboka.com)
- [antijantemiriam.com](https://antijantemiriam.com)
- [antijantepodden.com](https://antijantepodden.com)
- [billgoats.com](https://billgoats.com)
- [curious.art](https://curious.art)
- [curiouscreators.com](https://curiouscreators.com)
- [frodr.com](https://frodr.com)
- [klevstul.com](https://klevstul.com)

# How to run

`python jongleur.py -v /path/to/jongleur/site/config.cson`

View [run.sh](https://gitlab.com/pqq/jongleur-example/-/blob/main/site/run.sh) for an example.

View the [third party requirements](https://gitlab.com/pqq/jongleur/-/blob/master/readme.md#third-party-requirements).

# The Jongleur Example Site

The easiest way to learn how Jongleur works is by checking out the [Jongleur Example Site](https://gitlab.com/pqq/jongleur-example).

### File structure

Below is an overview of how the example site is organised. Some file / folder names are fixed (constant), and can't be changed. Other are changeable (mutable).

```
site/                                                               # 01 [mutable]
         content/                                                   # 02 [constant]
                 assets/                                            # 03 [constant]
                        chair.jpg                                   # 04 [mutable]
                 home/                                              # 05 [constant]
                      home.md                                       # 06 [constant]
                 sections/                                          # 07 [constant]
                          -noexport/                                # 08 [mutable]
                                    thisWillNotBeExported.md        # 09 [mutable]
                          .hidden/                                  # 10 [mutable]
                                  hardtofind/                       # 11 [mutable]
                                            i.md                    # 12 [mutable]
                                            assets/                 #    [mutable]
                                                   mzfo.html        #    [mutable]
                          books/                                    # 13 [mutable]
                                childrenoftime/                     # 14 [mutable]
                                               part1/               # 15 [mutable]
                                                     cot_.jpg       # 16 [mutable]
                                                     i.md           # 17 [mutable]
                                nineteeneightyfour/                 # 18 [mutable]
                                                   coverImage_.jpg  # 19 [mutable]
                                                   index.md         # 20 [mutable]
                          news/                                     # 21 [mutable]
                               210115_hello/                        # 22 [mutable]
                                            cover_.png              # 23 [mutable]
                                            index.md                # 24 [mutable]
                               210125_features/                     # 25 [mutable]
                                                -iii.md             # 26 [mutable]
                                                .ii.md              # 27 [mutable]
                                                cover_.png          # 28 [mutable]
                                                image_.jpg          # 29 [mutable]
                                                image.jpg           # 30 [mutable]
                                                image2_.jpg         # 31 [mutable]
                                                index.md            # 32 [mutable]
                               index.markdown                       # 33 [mutable]
        html_export/                                                # 34 [mutable]
                    assets/                                         # 35 [constant]
                    favicon/                                        # 36 [constant]
                    fonts/                                          # 37 [constant]
                    *.*                                             # 38 [mutable]
        layout/                                                     # 39 [constant]
               favicon/                                             # 40 [constant]
               fonts/                                               # 41 [constant]
               feed.json                                            # 42 [constant]
               index.html                                           # 43 [constant]
               robots.txt                                           # 44 [constant]
               rssfeed.xml                                          # 45 [constant]
               sitemap.xml                                          # 46 [constant]
               styles.css                                           # 47 [constant]
        config.cson                                                 # 48 [constant]
        run.sh                                                      # 49 [mutable]
```

**01** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site) : The root folder of the jongleur site. The site's settings and content are stored in this folder. The program itself (the code), is stored outside this directory.

**02** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content) : The root of the web content.

**03** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/assets) : A folder that typically can be used for content that is not specific to a single post.

**05** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/home) : Containing folder for the file that defines the home page, or front page, for the site.The home page, or the front page, of the site.

**06** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/home/home.md) : Content for the site's home page / front page.

**07** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/) : Contains the different sections of the site. It is here the main content is stored. Each sub-folder (section) becomes an entry in the menu.

**08** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/-noexport) : Since the name of the folder starts with a dash (-), this folder is never exported to HTML by Jongleur. Hence, the content inside this folder won't automatically become a part of the generated web site.

**10** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/.hidden) : When a folder starts with a dot (.) its content will be "hidden", and you need to know the exact link to access it. Do note that the sitemap.xml will contain links to "hidden" files.

**11** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/.hidden/occultare) : Since the parent folder starts with a dot, the folder's content will be hidden.

**12** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/.hidden/occultare/i.md) : This content is hidden due to the facts the parent (grandparent) folder starts with a dot. However, it is generated into a HTML page. Jongleur tries to generate HTML out of all `.md` (Markdown) files, unless they start with a dash (-) or is inside a folder starting with a dash.

**13** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/books) : "Books" will become a section of the site, with a menu link.

**14** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/books/childrenoftime) : One way of organising "books" is by title. Here is an example of that.

**15** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/books/childrenoftime/part1) : You can have "unlimited" number of sub folders, which gives you several ways of categorising the content.

**16** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/books/childrenoftime/part1/cot_.jpg) : To avoid the "photoPrepper" from altering the photo a underscore (_) is added to the end of the file name (before the file extension).

**17** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/books/childrenoftime/part1/i.md) : Markdown files must be formatted in a special way, for Jongleur to read them as proper posts. View [the source](https://gitlab.com/pqq/jongleur/-/raw/master/example_site/content/sections/books/childrenoftime/part1/i.md) for details.

**21** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/news/) : "News" becomes another section, and hence another entry in the menu.

**26** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/news/210125_features/-iii.md) : Since the file name starts with a dash (-), the content is not exported into a HTML page.

**27** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/news/210125_features/.ii.md) : Since the file name starts with a dot (.), the content is "hidden".

**29** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/news/210125_features/image_.jpg) : Example of an image that has been parsed by the "photoPrepper". Here a watermark has been added.

**33** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/content/sections/news/index.markdown) : If a `index.markdown` is put directly inside the first sub-folder of `sections/`, it's content is presented at the top of the section's listing page.

**34** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/html_export) : The generated web site is exported into this folder.

**39** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout) : The data defining the layout of the site is stored inside this folder.

**40** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout/favicon) : The site's favicon.

**41** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout/fonts) : The site's fonts.

**42** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout/feed.json) : The template file used for generating the JSON feed.

**43** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout/index.html) : The template file used for generating all HTML pages.

**44** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout/robots.txt) : The `robots.txt` file used for the site.

**45** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout/rssfeed.xml) : The template file used for generating the RSS feed.

**46** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout/sitemap.xml) : The template file used for generating the sitemap.

**47** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/layout/styles.css) : The site's style sheet.

**48** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/config.cson) : The Jongleur configuration file.

**49** : [View](https://gitlab.com/pqq/jongleur-example/-/tree/main/site/run.sh) : Example of a bash file that can be used to run Jongleur.

### Configuration

Site specific settings are set in the [config.cson](https://gitlab.com/pqq/jongleur/-/blob/master/example_site/config.cson) file.

### Styling

To get a different feel and look, the [styles.css](https://gitlab.com/pqq/jongleur/-/blob/master/additional_resources/styles/styles.css) file can be altered.



# Tips and tricks when writing posts

### Image sizes

- A good size for banner/cover images is `1200 x 700 px`.
- A good max width for images in posts is from `1000 px` to `1200 px`.

### Features overview

[This page](https://pqq.gitlab.io/jongleur.example/news210125_featuresindex.html) presents an overview of the different features in action. See [the source](https://gitlab.com/pqq/jongleur/-/raw/master/example_site/content/sections/news/210125_features/index.md) for more information.



# Third Party Requirements

### BeautifulSoup4
- `python-beautifulsoup4` (Arch community package)
- https://www.crummy.com/software/BeautifulSoup/index.html
- https://pypi.org/project/beautifulsoup4/

### CSON
- `python-cson` (Arch extra package)
- https://github.com/avakar/pycson
- https://pypi.org/project/cson/

### Markdown
- `python-markdown` (Arch package)
- https://github.com/Python-Markdown/markdown
- https://pypi.org/project/Markdown/

## photoPrepper requirements

### FFmpeg
- `ffmpeg` (Arch community package)
- https://ffmpeg.org/

### exiftool
- `exiftool` (Arch AUR package)
- https://aur.archlinux.org/packages/exiftool
- https://www.exiftool.org

## ASCIIfy requirements

### pillow
- `python-pillow` (Arch community package)
- https://python-pillow.org/
- https://github.com/python-pillow/Pillow



# Frequently asked questions

### Q: Can the same `styles.css` file be used for multiple sites?

Yes. Create symbolic links to your main css file, from the layout directory for the site you want to "copy" to.

**Example:**  
`$ ln -s file1 link1`  
`$ ln -s /myhd/jongleur/additional_resources/styles/styles.css .`



# Developer Information

### Development OS

For developing this solution [Manjaro Linux](https://manjaro.org/) has been used. There should be no problem at all using a different OS, however, please notice that the information in this document might be a tad off.

### Atom IDE

- Install [Atom editor](https://atom.io/).
  - Backup: https://gist.github.com/introspectionism/9dd47b84a8819b08d4563c41181e4398
- Shortcuts: https://github.com/nwinkler/atom-keyboard-shortcuts
- Theme: https://gitlab.com/pqq/neon-krome-syntax

### Pylint

- install [pylint](https://pylint.org/) through `PacUI` etc.
- Pylint features and exceptions:
  - https://pylint.pycqa.org/en/latest/technical_reference/features.html

### NGINX

- Install [NGINX](https://www.nginx.com/) through [Pamac](https://wiki.manjaro.org/index.php?title=Pamac) or `PacUI`

#### Nifty NGINX commands

```
$ sudo nano /etc/nginx/nginx.conf
$ sudo nginx
$ sudo nginx -s reload
$ sudo nginx -s quit
$ ps aux | grep -i 'nginx'
```

#### URL to running instance

- `http://127.0.0.1:8080/gitlab/jongleur/example_site/html_export/`

#### Root favicon

- Save `favicon.ico` to the webserver root folder
  - `/media/nas/cb/gitRepos/`

#### Troubleshooting

**`ERROR`**

Reason:  
- Description

Fix:  
```
$ how_to_fix_this
...
```

### References

- https://gitlab.com/pqq/jongleur/-/issues/15


# Versions overview

- [v1 Janker](https://gitlab.com/pqq/jongleur/-/tags/v1)
  - [browse files](https://gitlab.com/pqq/jongleur/-/tree/117a6f5e5cc085746b51347ecfb8490c1b9ac362)
