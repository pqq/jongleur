CURRENTDIR=${PWD##*/}

if [ "${CURRENTDIR}" != "src" ]; then
  echo "run this script from the 'src' directory"
  exit 1
fi

trash-put venv

python3 -m venv venv
source venv/bin/activate
pip install -r ../requirements/requirements-dev.txt
chmod 755 ../sh/*
../sh/pip_install.sh
#python -m build
