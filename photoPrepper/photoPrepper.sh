#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        prepares photos for publishing
# author:      njs
# started:     dec 2020
#
# requires:    ffmpeg
#              exiftool
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# ---
# set variables
# ---
#ffmpeg_maxwidth=1500
#watermark='NJS'
#watermark_font='/usr/share/fonts/adobe-source-code-pro/SourceCodePro-Light.otf'

# ---
# make sure we're not root (no need for that)
# ---
if [ "$EUID" -eq 0 ]
  then echo "error: don't run as root, please"
  exit
fi

# ---
# print the name of this file
# ---
#this_file_name=`basename "$0"`
#echo 'Running '$this_file_name

# ---
# make sure we have needed arguments
# ---
if [ $# -ne 5 ]; then
    echo usage: $this_file_name [path_to_font] [watermark] [maxwidth] [maxheight] [path_to_image_file]
    exit 1
fi

watermark_font=$1
watermark=$2
ffmpeg_maxwidth=$3
ffmpeg_maxheight=$4
file_path_full=$5

# ---
# make sure file exists
# ---
if [ ! -f "$file_path_full" ]; then
    echo "ERROR: photoPrepper.sh: Non existing file: $file_path_full"
    exit 1
fi

# ---
# get paths and file names
# ---
path=${file_path_full%/*}
file_name_full=`basename "$file_path_full"`
extension="${file_name_full##*.}"
file_name_no_extension="${file_name_full%.*}"
tmp_append="_tmp."
append="_."
out_file_tmp=$path/$file_name_no_extension$tmp_append$extension
out_file=$path/$file_name_no_extension$append$extension

# ---
# scale image
# ---
ffmpeg -y -loglevel quiet -i $file_path_full -vf "scale='if(gt(a,min($ffmpeg_maxwidth,iw)/min($ffmpeg_maxheight,ih)),min($ffmpeg_maxwidth,iw),-1)':'if(gt(a,min($ffmpeg_maxwidth,iw)/min($ffmpeg_maxheight,ih)),-1,min($ffmpeg_maxheight,ih))'" $out_file_tmp

# ---
# add watermark to image
# ---
# loglevel:
#    quiet (for production), verbose (for debugging)
# box:
#  Used to draw a box around text using the background color. The value must be either 1 (enable)
#  or 0 (disable). The default value of box is 0.
# boxborderw:
#  Set the width of the border to be drawn around the box using boxcolor. The default value of
#  boxborderw is 0.
# boxcolor:
#  The color to be used for drawing box around text. The default value of boxcolor is “white”.
# line_spacing:
#  Set the line spacing in pixels of the border to be drawn around the box using box. The default
#  value of line_spacing is 0.
ffmpeg -y -loglevel quiet -i $out_file_tmp -vf "drawtext=fontfile=$watermark_font:
   text=$watermark:
   fontcolor=0x000000:box=1:boxcolor=0xffffff@0.5:boxborderw=5:line_spacing=32:
   x=W-tw-10:y=H-th-10" $out_file

# remove the temp file
rm $out_file_tmp

# ---
# set exif data
# ---
camera_model=$watermark
timestamp=`date +"%Y:%m:%d %T"`
software="Jongleur photoPrepper"
exiftool -Copyright="$watermark" -Model="$watermark" -Software="$software" -DateTimeOriginal="$timestamp" -overwrite_original $out_file &> /dev/null
