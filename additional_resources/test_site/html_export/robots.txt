# /robots.txt
#
# https://en.wikipedia.org/wiki/Robots_exclusion_standard
# https://www.robotstxt.org/robotstxt.html

# exclude all robots
User-agent: *
Disallow: /

# allow all robots
#User-agent: *
#Disallow:

