title:      Issue 54
date:       2021.01.25 17:24
published:  true
comments:   false
image: 	    corbett_.png
~~~
Test content
~~~
[e:issue](https://gitlab.com/pqq/jongleur/-/issues/54)

---

all valid image paths for the header:
```
image:      /content/sections/tests/issue54/corbett_.png
image: 	    /tests/issue54/corbett_.png
image: 	    corbett_.png
```

#### Images

`corbett_.png`:
![](corbett_.png)

`assets/odysee_.png`:
![](assets/odysee_.png)

#### JTAG "LIP"

`LIP=/content/sections/journal/.210125_contact/_i.md`:
{{LIP=/content/sections/journal/.210125_contact/_i.md}}

`LIP=/journal/.210125_contact/_i.md`:
{{LIP=/journal/.210125_contact/_i.md}}

#### Link to internal content

`/journal/.210125_contact/_i.md`:
[i:Contact](/journal/.210125_contact/_i.md)

